#!/bin/bash
. config

echo Current installed rndnet modules: 
pip3 list | grep -e rndnet- -e bfj
echo 

date_old=$(stat -c %y $SCHEME_FILE)

#--------------------------------------------
for i in "${arr[@]}"
do
    echo Update rndnet/$i sources
    git --git-dir=$src/$i/.git --work-tree=$src/$i/ pull origin master
done

#---------------------------------------------

for i in "${arr[@]}"
do
  echo Install rndnet/$i    
  cd $src/$i

  if [ $INSTALL_REQ = "true" ]
  then
      sudo `which pip3` install -r requirements.txt
  fi
  sudo `which pip3` install . -U 
  
  #python setup.py bdist_wheel
  #sudo pip3 install dist/*.whl
  
  #rm -r dist/
  #rm -r build/
done

date_new=$(stat -c %y $SCHEME_FILE)
if [ "$date_old" != "$date_new" ] ; then
    echo -e "\n ${SCHEME_FILE} has changed! You must update database!"
fi

#---------------------------------------------

echo
echo Current installed rndnet modules: 
pip3 list | grep -e rndnet- -e bfj
