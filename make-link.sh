#!/bin/bash
echo Create links to this scripts

target_dir=$(pwd)
read -e -p "Target dir:" -i ""${target_dir} target_dir 

cur_dir=$(pwd)
mkdir -p ${target_dir}

cd  ${target_dir}
ln -sv ${cur_dir}/install-update-all.sh
ln -sv ${cur_dir}/check-status.sh
ln -sv ${cur_dir}/uninstall-all.sh
ln -sv ${cur_dir}/devenv.sh
ln -sv ${cur_dir}/update_database.sh
ln -sv ${cur_dir}/clone-all.sh
ln -sv ${cur_dir}/minio_manager.sh
ln -sv ${cur_dir}/rndnet-init-db.sh
ln -sv ${cur_dir}/rabbitmq-install.sh


cat << 'EOF' > config

CLOUD_NAME=local
INSTALL_REQ=true

src=$(pwd)
arr=( common bfj.py server scheduler client)
arr_full=( rndnet-common bfj rndnet-server rndnet-scheduler rndnet-client)

SCHEME_FILE=$src/server/rndnet_server/console/schema.py
EOF



