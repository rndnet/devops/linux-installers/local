#!/bin/bash

clear


BF_ADMIN_PASS=pass
BF_USER_PASS=pass
CLOUD_MAIN_DATABASE_NAME=cloud_cloud1

SUDO_COMMAND=sudo
CUR_USER=$USER
if [ "$CUR_USER" = 'root' ]
then
    SUDO_COMMAND=
fi

#------------------------------------------------------------------------------------
function set_pg10_params() {
  PG10_YUM_RPM=pgdg-redhat-repo-latest.noarch.rpm
  PG10_YUM=http://download.postgresql.org/pub/repos/yum/10/redhat/rhel-7-x86_64/${PG10_YUM_RPM}

  PG10_PACKAGES="postgresql10.x86_64 postgresql10-contrib postgresql10-plpython"
  PG10_SERVICE=postgresql-10
  PG10_CONF=/var/lib/pgsql/10/data/pg_hba.conf
  PG10_INIT="/usr/pgsql-10/bin/postgresql-10-setup initdb"

  PG_YUM_RPM=${PG10_YUM_RPM}
  PG_YUM=${PG10_YUM}
  PG_PACKAGES=${PG10_PACKAGES}
  PG_SERVICE=${PG10_SERVICE}
  PG_CONF=${PG10_CONF}
  PG_INIT=${PG10_INIT}
}

function pg_install {
  echo Install ${PG_SERVICE}
  #curl -O ${PG_YUM}
  #$SUDO_COMMAND yum install -y ${PG_YUM_RPM}

  $SUDO_COMMAND yum install -y ${PG_YUM}
  $SUDO_COMMAND yum install -y ${PG_PACKAGES}

  $SUDO_COMMAND ${PG_INIT}

  $SUDO_COMMAND systemctl start ${PG_SERVICE}
}


function user_create() {
  echo Create users
  read -e -p "Enter bf_admin password: " -i ${BF_ADMIN_PASS} BF_ADMIN_PASS
  read -e -p "Enter bf_user password: " -i ${BF_USER_PASS} BF_USER_PASS

  $SUDO_COMMAND su - postgres << BASH
  createuser -s -d -w bf_admin
  createuser -S -D -R -w bf_user
  psql -c "alter  user bf_admin with password '${BF_ADMIN_PASS}';"
  psql -c "alter  user bf_user with password '${BF_USER_PASS}';"
BASH
}


function set_access() {
 echo Change access ${PG_CONF}: peer or trust or ident  to  md5
 $SUDO_COMMAND cp  ${PG_CONF} ${PG_CONF}_bak
 $SUDO_COMMAND sed -i '/local/s/peer/md5/'  ${PG_CONF}
 $SUDO_COMMAND sed -i '/local/s/trust/md5/' ${PG_CONF}
 $SUDO_COMMAND sed -i '/local/s/ident/md5/' ${PG_CONF}
 $SUDO_COMMAND sed -i '/host/s/peer/md5/'   ${PG_CONF}
 $SUDO_COMMAND sed -i '/host/s/trust/md5/'  ${PG_CONF}
 $SUDO_COMMAND sed -i '/host/s/ident/md5/'  ${PG_CONF}
}

function set_remote_access() {
  echo Enable remote access
  echo '# Remote access ' |  $SUDO_COMMAND tee -a ${PG_CONF} > /dev/null
  echo 'host    all             all              0.0.0.0/0              md5  ' |  $SUDO_COMMAND tee -a ${PG_CONF} > /dev/null
  echo 'host    all             all              ::/0                   md5  ' |  $SUDO_COMMAND tee -a ${PG_CONF} > /dev/null
}

function pg_enable() {
  echo Autostart postgresql
  $SUDO_COMMAND systemctl enable  ${PG_SERVICE}
}

function pg_restart() {
 echo Restart postgresql
 $SUDO_COMMAND systemctl restart  ${PG_SERVICE}
}

function cloud_create() {
  echo Create cloud database
  read -e -p "Enter cloud main database name: " -i ${CLOUD_MAIN_DATABASE_NAME} CLOUD_MAIN_DATABASE_NAME
  echo "createdb "${CLOUD_MAIN_DATABASE_NAME}" -U bf_admin"
  echo "Input bf_admin password below"
  createdb ${CLOUD_MAIN_DATABASE_NAME} -U bf_admin
}

function cloud_check() {
  echo Check connect to cloud database
  read -e -p "Enter cloud main database name: " -i ${CLOUD_MAIN_DATABASE_NAME} CLOUD_MAIN_DATABASE_NAME
  echo "psql -U bf_admin -d "${CLOUD_MAIN_DATABASE_NAME}" -c '\l ;\q'"
  psql -U bf_admin -d ${CLOUD_MAIN_DATABASE_NAME} -c "\l ;\q"
  echo "psql -U bf_user -d "${CLOUD_MAIN_DATABASE_NAME}" -c '\l ;\q'"
  psql -U bf_user -d ${CLOUD_MAIN_DATABASE_NAME} -c "\l ;\q"
}

#------------------------------------------------------------------------------------
set_pg10_params

while [ 1 = 1 ]
do
echo "------------------------------------------"
echo "1) Install "${PG_SERVICE}
echo "2) Create rndnet database users: bf_admin and bf_user"
echo "3) Set access in postgresql config file " ${PG_CONF} "(peer or trust or ident to md5) and restart ${PG_SERVICE}"
echo "4) Enable remote access"
echo "5) Autostart "${PG_SERVICE}
echo "6) Restart "${PG_SERVICE}
echo "7) Create main cloud database as '"${CLOUD_MAIN_DATABASE_NAME}"'"
echo "8) Check connect to main cloud database as '"${CLOUD_MAIN_DATABASE_NAME}"'"
echo "------------------------------------------"
echo "14) Exit"

read doing # читаем в переменную $doing со стандартного ввода
case $doing in
1)
    echo
    pg_install
    ;;
2)
    echo
    user_create
    ;;
3)
    echo
    set_access
    pg_restart
    ;;
4)
    echo
    set_remote_access
    pg_restart
    ;;
5)
    echo
    pg_enable
    ;;
6)
    echo
    pg_restart
    ;;
7)
    echo
    cloud_create
    ;;
8)
    echo
    cloud_check
    ;;
14)
    echo "Bye!"
    exit 0
    ;;
*)
    echo "Unknown operation number!"
esac
done
