#!/bin/bash

. config

for i in "${arr[@]}"
do
    echo Clone rndnet/$i sources
    git clone git@gitlab.com:rndnet/apps/${i}.git
done
