#!/bin/bash
. config

echo Current installed rndnet modules: 
pip3 list | grep -e rndnet- -e bfj
echo 

for i in "${arr_full[@]}"
do
    echo Uninstall $i
    sudo pip3 uninstall -y $i
done

echo
echo Current installed rndnet modules: 
pip3 list | grep -e rndnet- -e bfj
