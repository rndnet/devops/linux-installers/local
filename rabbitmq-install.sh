set -vx
echo "Official install page: https://www.rabbitmq.com/install-rpm.html#package-cloud "

echo
echo "Install rabbitmq repository"
sudo rpm --import https://packagecloud.io/rabbitmq/rabbitmq-server/gpgkey
sudo rpm --import https://packagecloud.io/gpg.key
curl -s https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.rpm.sh | sudo bash

echo
echo "Install erlang repository"
curl -s https://packagecloud.io/install/repositories/rabbitmq/erlang/script.rpm.sh | sudo bash

echo
echo "Install RabbitMQ"
sudo yum install -y rabbitmq-server

echo
echo "Install rabbitmqadmin (optional)"
wget https://raw.githubusercontent.com/rabbitmq/rabbitmq-management/v3.8.x/bin/rabbitmqadmin 
chmod +x rabbitmqadmin
sudo chown root:root rabbitmqadmin
sudo mv rabbitmqadmin /usr/bin/rabbitmqadmin
sudo rabbitmq-plugins enable rabbitmq_management

echo
echo "Run rabbimq service"
sudo systemctl enable rabbitmq-server.service
sudo systemctl start rabbitmq-server 
systemctl status rabbitmq-server

set +vx

echo
echo "Congfiguration"
echo "Change limits https://www.rabbitmq.com/install-rpm.html#kernel-resource-limits"
echo "Production checklist:  https://www.rabbitmq.com/production-checklist.html"
echo
echo "User creation"
echo "https://www.rabbitmq.com/access-control.html"
echo " https://www.rabbitmq.com/rabbitmqctl.8.html#User_Management"
echo "If you do not want user standart guest user or rndnet_server and rabbitmq on different hosts. "
echo
echo "User creation example"
echo "sudo rabbitmqctl add_user restserver pass"
echo "sudo rabbitmqctl set_permissions -p / restserver ".*" ".*" ".*""
echo
echo "Admin creation example"
echo "sudo rabbitmqctl add_user mqadmin pass"
echo "sudo rabbitmqctl set_user_tags mqadmin administrator"
echo "sudo rabbitmqctl set_permissions -p / mqadmin ".*" ".*" ".*"  # objects  for: conf  write   read"



