#!/bin/bash
dir=$(cd $(dirname "${BASH_SOURCE}") > /dev/null; pwd -P)
for f in $(find ${dir} -mindepth 2 -maxdepth 2 -name devenv.sh); do
    source ${f}
done
