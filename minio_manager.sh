#!/bin/bash


#-------------------------------------------------------
function create_minio_data_folder {
   if [ -z ${MINIO_VOLUMES} ]
   then
      MINIO_VOLUMES=/minio_data
   fi

   read -e -p 'Enter MINIO DATA DIR: ' -i "${MINIO_VOLUMES}"  MINIO_VOLUMES
   sudo mkdir -pv ${MINIO_VOLUMES}
}

function set_minio_opts {
  if [ -z ${MINIO_OPTS} ]
   then
      MINIO_OPTS="--address :9001"
  fi
   
  read -e -p 'Enter MINIO OPTIONS: ' -i "${MINIO_OPTS}" MINIO_OPTS
}

function set_minio_access_key {
   if [ -z ${MINIO_ACCESS_KEY} ]
   then
      MINIO_ACCESS_KEY=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
   fi

   read -e -p 'Enter MINIO ACCESS KEY: ' -i "${MINIO_ACCESS_KEY}"  MINIO_ACCESS_KEY
}

function set_minio_secret_key {
   if [ -z ${MINIO_SECRET_KEY} ]
   then
      MINIO_SECRET_KEY=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
   fi

   read -e -p 'Enter MINIO SECRET KEY: ' -i "${MINIO_SECRET_KEY}"  MINIO_SECRET_KEY
}

function check_minimall_minio_params {
  if [ -z "${MINIO_VOLUMES}" ]
  then
   create_minio_data_folder
  fi
  if [ -z "${MINIO_OPTS}" ]
  then
   set_minio_opts
  fi

}

function check_all_minio_params {
  check_minimall_minio_params 

  if [ -z "${MINIO_ACCESS_KEY}" ]
  then
   set_minio_access_key
  fi
  if [ -z "${MINIO_SECRET_KEY}" ]
  then
   set_minio_secret_key
  fi
}

function set_minio_service_user {

  if [ -z "${MINIO_SERVICE_USER}" ]
  then
   MINIO_SERVICE_USER=minio-user
  fi
  
  read -e -p 'Enter MINIO SERVICE USER: ' -i "${MINIO_SERVICE_USER}"  MINIO_SERVICE_USER

  if [ -z "${MINIO_SERVICE_GROUP}" ]
  then
   MINIO_SERVICE_GROUP=${MINIO_SERVICE_USER}
  fi
   
  read -e -p 'Enter MINIO SERVICE GROUP: ' -i "${MINIO_SERVICE_GROUP}"  MINIO_SERVICE_GROUP
}
#-------------------------------------------------------


CUR_USER=$USER
#RUN_DIR=$(pwd)
#CUR_GROUP=$(id -g -n $USER)

if [ $CUR_USER = 'root' ]
then
    echo "'root' user not recommended!"
    exit 1
fi

echo
echo
echo "Found configuration files in current directory"
ls -1 *.cfg 2>/dev/null
echo

read -e -p 'Enter minio service name: ' -i 'minio'  MINIO_SERVICE
if [ -f "${MINIO_SERVICE}.cfg" ] 
then
   . ${MINIO_SERVICE}.cfg
fi
if [ -f "/etc/default/${MINIO_SERVICE}.cfg" ] 
then
   MINIO_SERVICE_USER=$(stat -c %U /etc/default/${MINIO_SERVICE}.cfg) 
   MINIO_SERVICE_GROUP=$(stat -c %G /etc/default/${MINIO_SERVICE}.cfg) 
fi

#read -e -p 'Enter this manager output dir: ' -i $HOME/rndnet_server WORKDIR
#mkdir -p $WORKDIR
#cd $WORKDIR


while [ 1 = 1 ]
do
echo "------------------------------------------"
echo 'Current '${MINIO_SERVICE}' params:'
echo 'MINIO_VOLUMES='${MINIO_VOLUMES}
echo 'MINIO_OPTS="'${MINIO_OPTS}'"'
echo 'MINIO_ACCESS_KEY='${MINIO_ACCESS_KEY}
echo 'MINIO_SECRET_KEY='${MINIO_SECRET_KEY}
echo 'MINIO_SERVICE_USER='${MINIO_SERVICE_USER}
echo 'MINIO_SERVICE_GROUP='${MINIO_SERVICE_GROUP}
echo "------------------------------------------"
echo "Choose operation:"
echo "------------------------------------------"
echo "1) Download minio server"
echo "2) Install minio server (copy to /usr/bin)"
echo "3) Create minio data folder "
echo
echo "4) Test run minio with current MINIO_VOLUMES and MINIO_OPT (user input if any empty)"
echo "5) Test run minio with current MINIO_VOLUMES, MINIO_OPTS, MINIO_ACCESS_KEY, MINIO_SECRET_KEY (user input if any empty)"
echo
echo "6) Create "${MINIO_SERVICE}".cfg configuration file with current MINIO_VOLUMES, MINIO_OPTS, MINIO_ACCESS_KEY, MINIO_SECRET_KEY (user input if any empty)"
echo "7) Create minio user"
echo "8) Create "${MINIO_SERVICE}".service file"
echo "9) Install "${MINIO_SERVICE}".service and copy "${MINIO_SERVICE}".cfg to /etc/default/"${MINIO_SERVICE}
echo "10) Stop and uninstall "${MINIO_SERVICE}".service and remove /etc/default/"${MINIO_SERVICE}
echo
echo "11) Start "${MINIO_SERVICE}".service"
echo "12) Stop "${MINIO_SERVICE}".service"
echo "13) Status "${MINIO_SERVICE}".service"
echo "------------------------------------------"
echo "14) Exit"

read doing # читаем в переменную $doing со стандартного ввода
case $doing in
1)
   echo 
   echo "Download minio"
   wget https://dl.min.io/server/minio/release/linux-amd64/minio
   ;;
2)   
   echo 
   echo  "Install minio (copy to /usr/bin)"
   chmod +x -v minio
   sudo chown -v root:root minio
   sudo mv -v minio /usr/bin
   ;;
3)
   echo 
   echo "Create minio data folder"
   create_minio_data_folder
   ;;
4)

  echo 
  echo "Test run minio "

  check_minimall_minio_params 

  minio server ${MINIO_VOLUMES} ${MINIO_OPTS}    
  ;;
5)
  echo "Test run minio with keys "
  check_all_minio_params 
  
  export MINIO_ACCESS_KEY=${MINIO_ACCESS_KEY}
  export MINIO_SECRET_KEY=${MINIO_SECRET_KEY}
  minio server ${MINIO_VOLUMES} ${MINIO_OPTS}    

  ;;
6)
  echo "Create "${MINIO_SERVICE}".cfg configuration file"
  check_all_minio_params 

  cat << EOT > ${MINIO_SERVICE}.cfg
MINIO_VOLUMES="${MINIO_VOLUMES}"
MINIO_OPTS="${MINIO_OPTS}"
MINIO_ACCESS_KEY=${MINIO_ACCESS_KEY}
MINIO_SECRET_KEY=${MINIO_SECRET_KEY}  
EOT

  cat  ${MINIO_SERVICE}.cfg
  ;;

7)
  set_minio_service_user
  id -u ${MINIO_SERVICE_USER} &>/dev/null || sudo adduser  ${MINIO_SERVICE_USER}
  ;;

8)
  echo "7) Create" ${MINIO_SERVICE}".service file"

  if [ -z "${MINIO_SERVICE_USER}" ]
  then
      set_minio_service_user
  fi

cat <<EOF > ${MINIO_SERVICE}.service
[Unit]
Description=Minio
Documentation=https://docs.minio.io
Wants=network-online.target
After=network-online.target
AssertFileIsExecutable=/usr/bin/minio

[Service]
Type=simple

##WorkingDirectory=/usr/

User=${MINIO_SERVICE_USER}
Group=${MINIO_SERVICE_GROUP}

#PermissionsStartOnly=true

EnvironmentFile=/etc/default/${MINIO_SERVICE}.cfg
ExecStartPre=/bin/bash -c "[ -n \"\${MINIO_VOLUMES}\" ] || echo \"Variable MINIO_VOLUMES not set in /etc/default/${MINIO_SERVICE}.cfg\""

ExecStart=/usr/bin/minio server \$MINIO_OPTS \$MINIO_VOLUMES

StandardOutput=journal
StandardError=inherit

# Specifies the maximum file descriptor number that can be opened by this process
LimitNOFILE=65536

# Disable timeout logic and wait until process is stopped
TimeoutStopSec=0

# SIGTERM signal is used to stop Minio
KillSignal=SIGTERM
SendSIGKILL=no
SuccessExitStatus=0

[Install]
WantedBy=multi-user.target
EOF

  echo
  cat  ${MINIO_SERVICE}.service
  ;;

9)
    echo    
    echo "Install "${MINIO_SERVICE}".service and copy "${MINIO_SERVICE}".cfg to /etc/default/"${MINIO_SERVICE}".cfg"
    
    if [ ! -f "${MINIO_SERVICE}.cfg" ] 
    then
       echo ${MINIO_SERVICE}".cfg file not found!" 
    else
    
        if [ ! -f "${MINIO_SERVICE}.service" ] 
        then
           echo ${MINIO_SERVICE}".service file not found!" 
        else

           if [ -z "${MINIO_SERVICE_USER}" ]
           then
                set_minio_service_user
           fi

           sudo chown -Rv ${MINIO_SERVICE_USER}:${MINIO_SERVICE_GROUP} ${MINIO_VOLUMES}

           sudo cp -vu ${MINIO_SERVICE}.cfg /etc/default/${MINIO_SERVICE}.cfg
           sudo chown -v ${MINIO_SERVICE_USER}:${MINIO_SERVICE_GROUP} /etc/default/${MINIO_SERVICE}.cfg

           sudo cp -v ${MINIO_SERVICE}.service /etc/systemd/system/${MINIO_SERVICE}.service

           sudo systemctl enable ${MINIO_SERVICE}.service
           sudo systemctl start ${MINIO_SERVICE}.service
           systemctl status ${MINIO_SERVICE}.service
        fi
    fi

    ;;
10)
    echo
    echo "Stop and uninstall "${MINIO_SERVICE}".service and remove /etc/default/"${MINIO_SERVICE}

    sudo systemctl stop ${MINIO_SERVICE}.service
    sudo systemctl disable ${MINIO_SERVICE}.service

    sudo  rm -v  /etc/systemd/system/${MINIO_SERVICE}.service 
    sudo  rm -v /etc/default/${MINIO_SERVICE}.cfg

    sudo systemctl daemon-reload

    ;;
11)
    echo
    echo "Start "${MINIO_SERVICE}".service"
    sudo  systemctl start  ${MINIO_SERVICE}.service
    systemctl status ${MINIO_SERVICE}.service
    ;;
12)
    echo
    echo "Stop "${MINIO_SERVICE}".service"
    sudo systemctl stop ${MINIO_SERVICE}.service
    systemctl status ${MINO_SERVICE}.service
    ;;
13)
    echo
    echo "Status "${MINIO_SERVICE}".service"
    systemctl status ${MINIO_SERVICE}.service
    ;;
14)
    echo "Bye!"
    exit 0
    ;;
*)
    echo "Unknown operation number!"
esac

echo

done
